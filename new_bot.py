import telebot
from telebot import types
import mysql.connector
from random import random
from random import shuffle
from openpyxl import load_workbook
from datetime import datetime

bot = telebot.TeleBot("765264987:AAHvB8lGFWXNPWeFoo6JQXIDPTDsC53njSM")
hello_text = "Hello!!!"

@bot.message_handler(commands=['start'])
def fisrt_message(message):
    bot.send_message(message.chat.id, hello_text, reply_markup=keyboard())

def keyboard():
    markup = types.ReplyKeyboardMarkup(one_time_keyboard = True, resize_keyboard = True)
    itembtn1 = types.KeyboardButton('Привет')
    markup.add(itembtn1)
    return markup

def keyboard_new():
    markup_new = types.ReplyKeyboardMarkup(row_width=2 ,one_time_keyboard = True, resize_keyboard = True)
    itembt = types.KeyboardButton('Создать новое расписание')
    itembt1 = types.KeyboardButton('Вывести расписание института')
    itembt2 = types.KeyboardButton('Добавить бота в беседу')
    itembt3 = types.KeyboardButton('Вывод готовых расписаний')
    itembt4 = types.KeyboardButton('Редактировать созданное вами расписание')
    itembt5 = types.KeyboardButton('Сохранить данные')
    itembt6 = types.KeyboardButton('Игрульки')
    markup_new.add(itembt, itembt1, itembt2, itembt3, itembt4, itembt5, itembt6)
    return markup_new

def keyboard_tablet():
    tablet = types.ReplyKeyboardMarkup(row_width=2 ,one_time_keyboard = True, resize_keyboard = True)
    button1 = types.KeyboardButton('Студент')
    button2 = types.KeyboardButton('Преподаватель')
    button3 = types.KeyboardButton('Назад')
    tablet.add(button1, button2, button3)
    return tablet

def keyboard_edit():
    edit = types.ReplyKeyboardMarkup(row_width=2 ,one_time_keyboard = True, resize_keyboard = True)
    Button1 = types.KeyboardButton('Изменить')
    Button2 = types.KeyboardButton('Удалить')
    Button3 = types.KeyboardButton('Назад')
    edit.add(Button1, Button2, Button3)
    return edit

def keyboard_game():
    edit = types.ReplyKeyboardMarkup(row_width=2 ,one_time_keyboard = True, resize_keyboard = True)
    knopka1 = types.KeyboardButton('Угадай число')
    knopka2 = types.KeyboardButton('Битва с драконом')
    knopka3 = types.KeyboardButton('Помощь по игре "Угадай число"')
    knopka4 = types.KeyboardButton("Назад")
    edit.add(knopka1, knopka2, knopka3, knopka4)
    return edit

@bot.message_handler(content_types=['text'])
def text_message(message):
    if (message.text == 'Привет') or (message.text == 'привет'):
        bot.send_message(message.chat.id,"Хорошо. Что желаете?", reply_markup = keyboard_new())
    elif (message.text == 'Добавить бота в беседу'):
        bot.send_message(message.chat.id, "Обучение:\n1)Создать или состоять в беседе Telegram;\n2)Откройте группу и нажмите на её название сверху;\n3)Найдите пункт 'Добавить участника';\n4)В окне нажмите значок лупы и введите название добавляемого бота;\n5)Подтвердите согласие и программа появится в группе.")
    elif (message.text == 'Вывести расписание института'):
        bot.send_message(message.chat.id, "Выберите", reply_markup=keyboard_tablet())
    elif (message.text == 'Студент'):
        msg = bot.send_message(message.chat.id, "Введите номер своей группы.")
        bot.register_next_step_handler(msg, procces_raspisanie)
    elif (message.text == 'Преподаватель'):
        bot.send_message(message.chat.id, "Введите свои инициалы:\nПример: Пересунько П.В.")
    elif (message.text == 'Назад'):
        bot.send_message(message.chat.id, "Хорошо", reply_markup = keyboard_new())
    elif (message.text == 'Редактировать созданное вами расписание'):
        bot.send_message(message.chat.id, "Что хотите сделать?", reply_markup = keyboard_edit())
    elif (message.text == 'Игрульки'):
        bot.send_message(message.chat.id, "Выберите", reply_markup=keyboard_game())
    elif (message.text == 'Угадай число'):
        msg = bot.send_message(message.chat.id, "Компьютер загадал число от 0 до 100. Отгадайте его. У вас 10 попыток")
        bot.register_next_step_handler(msg, process_game1_step)
    elif (message.text == 'Битва с драконом'):
        msg = bot.send_message(message.chat.id, 'Введите номер двери (1, 2 или 3): ')
        bot.register_next_step_handler(msg, process_game2_step)
    elif (message.text == 'Помощь по игре "Угадай число"'):
        bot.send_message(message.chat.id, 'Самый верный способ отгадать число менее чем за 10 попыток - это делить диапазон на 2. Например, если число лежит в пределах от 0 до 100, то введя 50 мы сразу сокращаем диапазон поиска в два раза.')

i = 11
n = round(random() * 100)
def process_game1_step(message):
    global n, i
    if i > 10:
        i = 0
        n = round(random() * 100)
    while i <= 9:
        i += 1
        u = int(message.text)
        if u > n:
            msg = bot.send_message(message.chat.id, 'Много')
            bot.register_next_step_handler(msg, process_game1_step)
            return 
        elif u < n:
            msg = bot.send_message(message.chat.id, 'Мало')
            bot.register_next_step_handler(msg, process_game1_step)
            return
        else:
            bot.send_message(message.chat.id, 'Вы угадали с %d-й попытки' % i)
            i = 11
    else:
        bot.send_message(message.chat.id, 'Вы исчерпали 10 попыток. Было загадано' + " " + str(n))

doors = ['dragon', 'water', 'empty']
score = 0
lives = 3
def process_game2_step(message):
    global lives
    global score
    score += 100
    choice = int(message.text)
    shuffle(doors)
    if doors[choice - 1] == 'dragon':
        bot.send_message(message.chat.id, 'Вы сражались с драконом и потеряли жизнь.')
        lives -= 1
        msg = bot.send_message(message.chat.id, "Жизней соталось: " + " " + str(lives))
        bot.register_next_step_handler(msg, process_game2_step)
        return
    elif doors[choice - 1] == 'water':
        bot.send_message(message.chat.id, 'Вы выпили живой воды и получили жизнь.')
        lives += 1
        msg = bot.send_message(message.chat.id, "Жизней соталось: " + " " + str(lives))
        bot.register_next_step_handler(msg, process_game2_step)
        return
    else:
        bot.send_message(message.chat.id, 'Ничего не произошло.')
        msg = bot.send_message(message.chat.id, "Жизней соталось: " + " " + str(lives))
        bot.register_next_step_handler(msg, process_game2_step)
    if (lives == 0):
        bot.send_message(message.chat.id, 'Игра окончена. Ваш счет:' + " " + str(score))

def procces_raspisanie(message):
    wb = load_workbook('D:/lab/test.xlsx')
    sheet = wb.get_sheet_by_name('Лист1')
    sheet.title
    anotherSheet = wb.active
    anotherSheet
    sheet['A1'].value
    c = sheet['B2']
    c.row
    c.column
    c.coordinate
    sheet.cell(row=1, column=2).value

    current_date = datetime.today()
    weekday_dict = {"1": "Понедельник", "2": "Вторник", "3": "Среда", "4": "Четверг", "5": "Пятница", "6": "Суббота"}
    current_day_digit = current_date.isoweekday()
    current_day_str = weekday_dict.get(str(current_day_digit))
    bot.send_message(message.chat.id,current_day_str)

    group = message.text


    for cellObj in sheet['D2':'AL2']:
        for cell in cellObj:
            #print(cell.value, type(cell.value))
            if group in str(cell.value):
                group_cell_p = cell.coordinate
                group_column_letter = sheet[group_cell_p].column
                #print(f'Group cell: {group_cell_p}, {group_column_letter}')


    if current_day_str == "Понедельник":
        for i in [3, 6 , 9, 12, 15, 18, 21, 24, 27, 30]:
            if (not sheet.cell(row=i, column=group_column_letter).value and not sheet.cell(row=i + 1, column=group_column_letter).value and not sheet.cell(row=i + 2, column=group_column_letter).value):
                continue
            if (sheet.cell(row=i, column=group_column_letter).value):
                if i % 2 != 0:
                    bot.send_message(message.chat.id,"Нечётная неделя")
                    bot.send_message(message.chat.id,sheet.cell(row=i, column=2).value)
                else:
                    bot.send_message(message.chat.id,"Чётная неделя")
                    bot.send_message(message.chat.id,sheet.cell(row=i - 3, column=2).value)
                if sheet.cell(row=i, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i, column=group_column_letter).value)
                if sheet.cell(row=i + 1, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i + 1, column=group_column_letter).value)
                if sheet.cell(row=i + 2, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i + 2, column=group_column_letter).value + "\n")

    if current_day_str == "Вторник":
        for i in [33, 36, 39, 42, 45, 48, 51, 54, 57, 60]:
            if (not sheet.cell(row=i, column=group_column_letter).value and not sheet.cell(row=i + 1, column=group_column_letter).value and not sheet.cell(row=i + 2, column=group_column_letter).value):
                continue
            if (sheet.cell(row=i, column=group_column_letter).value):
                if i % 2 != 0:
                    bot.send_message(message.chat.id,"Нечётная неделя")
                    bot.send_message(message.chat.id,sheet.cell(row=i, column=2).value)
                else:
                    bot.send_message(message.chat.id,"Чётная неделя")
                    bot.send_message(message.chat.id,sheet.cell(row=i - 3, column=2).value)
                if sheet.cell(row=i, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i, column=group_column_letter).value)
                if sheet.cell(row=i + 1, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i + 1, column=group_column_letter).value)
                if sheet.cell(row=i + 2, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i + 2, column=group_column_letter).value + "\n")

    if current_day_str == "Среда":
        for i in [63, 66, 69, 72, 75, 78, 81, 84, 87, 90]:
            if (not sheet.cell(row=i, column=group_column_letter).value and not sheet.cell(row=i + 1, column=group_column_letter).value and not sheet.cell(row=i + 2, column=group_column_letter).value):
                continue
            if (sheet.cell(row=i, column=group_column_letter).value):
                if i % 2 != 0:
                    bot.send_message(message.chat.id,"Нечётная неделя")
                    bot.send_message(message.chat.id,sheet.cell(row=i, column=2).value)
                else:
                    bot.send_message(message.chat.id,"Чётная неделя")
                    bot.send_message(message.chat.id,sheet.cell(row=i - 3, column=2).value)
                if sheet.cell(row=i, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i, column=group_column_letter).value)
                if sheet.cell(row=i + 1, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i + 1, column=group_column_letter).value)
                if sheet.cell(row=i + 2, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i + 2, column=group_column_letter).value + "\n")

    if current_day_str == "Четверг":
        for i in [93, 96, 99, 102, 105, 108, 111, 114, 117, 120, 123, 126]:
            if (not sheet.cell(row=i, column=group_column_letter).value and not sheet.cell(row=i + 1, column=group_column_letter).value and not sheet.cell(row=i + 2, column=group_column_letter).value):
                continue
            if (sheet.cell(row=i, column=group_column_letter).value):
                if i % 2 != 0:
                    bot.send_message(message.chat.id,"Нечётная неделя")
                    bot.send_message(message.chat.id,sheet.cell(row=i, column=2).value)
                else:
                    bot.send_message(message.chat.id,"Чётная неделя")
                    bot.send_message(message.chat.id,sheet.cell(row=i - 3, column=2).value)
                if sheet.cell(row=i, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i, column=group_column_letter).value)
                if sheet.cell(row=i + 1, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i + 1, column=group_column_letter).value)
                if sheet.cell(row=i + 2, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i + 2, column=group_column_letter).value + "\n")

    if current_day_str == "Пятница":
        for i in [129, 132, 135, 138, 141, 144, 147, 150, 153, 156]:
            if (not sheet.cell(row=i, column=group_column_letter).value and not sheet.cell(row=i + 1, column=group_column_letter).value and not sheet.cell(row=i + 2, column=group_column_letter).value):
                continue
            if (sheet.cell(row=i, column=group_column_letter).value):
                if i % 2 != 0:
                    bot.send_message(message.chat.id,"Нечётная неделя")
                    bot.send_message(message.chat.id,sheet.cell(row=i, column=2).value)
                else:
                    bot.send_message(message.chat.id,"Чётная неделя")
                    bot.send_message(message.chat.id,sheet.cell(row=i - 3, column=2).value)
                if sheet.cell(row=i, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i, column=group_column_letter).value)
                if sheet.cell(row=i + 1, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i + 1, column=group_column_letter).value)
                if sheet.cell(row=i + 2, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i + 2, column=group_column_letter).value + "\n")

    if current_day_str == "Суббота":
        for i in [159, 162, 165, 168, 171, 174, 177, 180, 183, 186]:
            if (not sheet.cell(row=i, column=group_column_letter).value and not sheet.cell(row=i + 1, column=group_column_letter).value and not sheet.cell(row=i + 2, column=group_column_letter).value):
                continue
            if(sheet.cell(row=i, column=group_column_letter).value):
                if i % 2 != 0:
                    bot.send_message(message.chat.id,"Нечётная неделя")
                    bot.send_message(message.chat.id,sheet.cell(row=i, column=2).value)
                else:
                    bot.send_message(message.chat.id,"Чётная неделя")
                    bot.send_message(message.chat.id,sheet.cell(row=i-3, column=2).value)
                if sheet.cell(row=i, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i, column=group_column_letter).value)
                if sheet.cell(row=i + 1, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i + 1, column=group_column_letter).value)
                if sheet.cell(row=i + 2, column=group_column_letter).value:
                    bot.send_message(message.chat.id,sheet.cell(row=i + 2, column=group_column_letter).value + "\n")

if __name__ == "__main__":
    bot.polling(none_stop = True)